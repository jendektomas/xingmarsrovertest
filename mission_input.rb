require_relative 'rover.rb'

class MissionInput
  attr_accessor :mission_final_position
  @@mission_sequence = 0
  @@mission_area = []

  def initialize
    begin
      if @@mission_sequence == 0
        area = gets.chomp
        area = area.split(' ')
        @@mission_area = area
      else
        area = @@mission_area
      end

      # Get starting position
      start_position = gets.chomp
      start_position = start_position.split(' ')

      # Get orders for rover
      orders = gets.chomp
      orders = orders.split(//)

      # plateau area coordinates
      @x_x = area[0].to_i
      @y_y = area[1].to_i

      # mars rover starting coordinates
      @x = start_position[0].to_i
      @y = start_position[1].to_i

      # mars rover starting direction
      @actual_direction = start_position[2]

      # Create rover instance and start his journey
      rover = Rover.new(@x, @y, @x_x, @y_y, @actual_direction)
      @mission_final_position = rover.start_journey(orders)

      # Mission area sequence counter
      @@mission_sequence += 1
      @@mission_area = area
    rescue
      puts "Please input valid commands."
    end
  end
end
