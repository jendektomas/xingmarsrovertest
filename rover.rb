class Rover
  @@possible_directions = ['N', 'S', 'E', 'W']
  @@possible_moves = ['M']

  # Init coordinates - default coordinates if no input
  def initialize(x = 0, y = 0, x_x = 1, y_y = 1, actual_direction = 'N')
    @x = x
    @y = y
    @x_x = x_x
    @y_y = y_y
    @actual_direction = actual_direction
  end

  # Check if direction command from console is valid, if not - skip command
  def direction_check(direction)
    @@possible_directions.include? direction ? true : false
  end

  # Check if move command from console is valid, if not - skip command
  def move_check(move)
    @@possible_moves.include? move ? true : false
  end

  def turn_right
    if @actual_direction == 'N'
      @actual_direction = 'E'
    elsif @actual_direction == 'S'
      @actual_direction = 'W'
    elsif @actual_direction == 'W'
      @actual_direction = 'N'
    elsif @actual_direction == 'E'
      @actual_direction = 'S'
    end
  end

  def turn_left
    if @actual_direction == 'N'
      @actual_direction = 'W'
    elsif @actual_direction == 'S'
      @actual_direction = 'E'
    elsif @actual_direction == 'W'
      @actual_direction = 'S'
    elsif @actual_direction == 'E'
      @actual_direction = 'N'
    end
  end

  def move
    if @actual_direction == 'N' && @y < @y_y
      @y += 1
    elsif @actual_direction == 'S' && @y > 0
      @y -= 1
    elsif @actual_direction == 'W' && @x > 0
      @x -= 1
    elsif @actual_direction == 'E' && @x < @x_x
      @x += 1
    end
  end

  def process_order(input)
    return if direction_check(input) || move_check(input)

    case input
      when 'L'
        turn_left
      when 'R'
        turn_right
      when 'M'
        move
    end
  end

  def start_journey(orders)
    orders.each do |ord|
      process_order(ord)
    end

    return @x.to_s+" "+@y.to_s+" "+ @actual_direction
  end
end
