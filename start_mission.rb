# XING Mars Rover test 
#
# Tomas Jendek
# jendek.tomas@gmail.com
#
# Example Input
# 5 5
# 1 2 N
# LMLMLMLMM
# 3 3 E
# MMRMMRMRRM

require_relative 'mission_input.rb'

# Final output
@output = []

2.times do 
    rover_mission = MissionInput.new
    @output << rover_mission.mission_final_position
end

@output.each do |out|
    puts out
end








