### Private repository - XING Mars Rover Test ###

* Mars Rover Test - RUBY
* 1.0
* Author: Tomas Jendek
* Email: jendek.tomas@gmail.com

### Setup ###

* Run in ruby console file "start_mission.rb"
* Insert input to console

### Test ###

* Test available in file "test_rover.rb"