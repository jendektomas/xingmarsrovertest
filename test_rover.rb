require_relative "rover.rb"
require "test/unit"

class TestRover < Test::Unit::TestCase

  def rover_test
    x = 1
    y = 2
    x_x = 5
    y_y = 5
    actual_direction = 'N'

    rover = Rover.new(x, y, x_x, y_y, actual_direction)
    input = ['L', 'M', 'L', 'M', 'L', 'M', 'L', 'M', 'M']

    assert_equal("1 3 N", rover.process_order(input))
  end

end